package yurasik.com.simplebroadcastreceiver.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import yurasik.com.simplebroadcastreceiver.service.ScreenService;
import yurasik.com.simplebroadcastreceiver.util.LogUtil;

public class ScreenBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtil.i(intent.getAction() + " : " + intent.getExtras());
        Intent mIntent = new Intent(context, ScreenService.class);
        mIntent.setAction(intent.getAction());
        mIntent.putExtras(intent.getExtras());
        context.startService(mIntent);
    }

}
