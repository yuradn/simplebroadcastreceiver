package yurasik.com.simplebroadcastreceiver.service;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import yurasik.com.simplebroadcastreceiver.receivers.ScreenBroadcast;
import yurasik.com.simplebroadcastreceiver.util.LogUtil;

/**
 * Created by Yurii on 6/22/17.
 */

public class ScreenService extends Service {

    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();
    private ScreenBroadcast mScreenStateReceiver;
    private Listener mListener;
    private int isHeadset = -1;

    @Override
    public void onCreate() {
        super.onCreate();
        LogUtil.i("onCreate");
        mScreenStateReceiver = new ScreenBroadcast();
        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        screenStateFilter.addAction(Intent.ACTION_MEDIA_BUTTON);
        screenStateFilter.addAction(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(mScreenStateReceiver, screenStateFilter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent.getAction()!=null)
        switch (intent.getAction()) {
            case Intent.ACTION_HEADSET_PLUG:
                LogUtil.i("Headset action");
                Bundle bundle = intent.getExtras();
                LogUtil.i("Name: " + bundle.get("name"));
                int state = bundle.getInt("state");
                LogUtil.i("State: " + state);
                isHeadset = state;
                LogUtil.i("Microphone: " + bundle.get("microphone"));
                if (mListener!=null && isHeadset > -1) {
                    mListener.isAction(state > 0);
                }
                break;
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtil.i("onDestroy");
        unregisterReceiver(mScreenStateReceiver);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void setmListener(Listener mListener) {
        this.mListener = mListener;
        if (isHeadset > -1 && mListener!=null) {
            mListener.isAction(isHeadset > 0);
        }
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public ScreenService getService() {
            // Return this instance of LocalService so clients can call public methods
            return ScreenService.this;
        }
    }

}
